pluginManagement {
    repositories {
        gradlePluginPortal()
    }
    // Plugin Version Management
    val kotlinPluginVersion: String by settings
    plugins {
        kotlin("jvm") version kotlinPluginVersion
    }
}

dependencyResolutionManagement {
    repositories {
        mavenCentral()
    }
}

rootProject.name = "kfcup"

include("modules:framework")
include("modules:engine")
include("modules:sample-team")
include("modules:viewer")

